
--a. To find all artists that have letter D in their name: --
SELECT * FROM artists WHERE name LIKE '%D%';

--b. To find all songs that have a length of less than 230: --
SELECT * FROM songs WHERE length < 230;

--c. To join the 'albums' and 'songs' tables and only show the album name, song name, and song length:--
SELECT album_title as album_name, song_name, length FROM
albums JOIN songs ON albums.id = songs.album_id;

--d. To join the 'artists' and 'albums' tables and find all albums that have letter A in their name: --
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id
WHERE album_title LIKE '%A%';

--e. To sort the albums in Z-A order and show only the first 4 records: --
SELECT * FROM albums
ORDER BY album_title DESC
LIMIT 4;

--f. To join the 'albums' and 'songs' tables, sort albums from Z-A, and sort songs from A-Z: --
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id
ORDER BY album_title DESC, song_name ASC;